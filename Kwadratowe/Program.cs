﻿using System;

namespace Kwadratowe
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			WriteLine ("Czesc!\n");

			//#region Variables

			double a, b, c, delta, x1, x2;
			char yesNOtext;
			string readStr;
			
			//#regionend

			do {  //restart if yes
				do {  //while (delta < 0)

					Console.Clear ();
					Delay (1000);
					WriteLine ("Kwadratowe wyglonda: a(x^2) + bx + c\n");
					Delay (1000);
					WriteLine ("\t\t ('a' maje byc > za '0')\n");

					//#region ReadVarianles

					do {  //if linear
						Write ("\ta = ");
						a = ReadDouble ();
						if (a == 0) {
							Delay (1000);
							WriteLine ("\nError: Nie jest kwadratowe!");
						}
					} while (a < 0);

					Write ("\tb = ");
					b = ReadDouble ();
					Write ("\tc = ");
					c = ReadDouble ();
					
					//#regionend

					//delta
					delta = Delta (a, b, c);
					
					//chek delta *text
					if (delta < 0) {
						Delay (1000);
						WriteLine ("Nie umiem tego robic!");
						Delay (1000);
						WriteLine ("Zrob po nowej!");
					}

				} while (delta < 0);

				Delay (1000);
				WriteLine ("Delta = " + delta);
				
				//x1, x2
				x1 = ((-b) - Math.Sqrt (delta)) / (2 * a);
				x2 = ((-b) + Math.Sqrt (delta)) / (2 * a);

				WriteLine ("Resultat: \n\tx1 = " + x1 + ";\n\tx2 = " + x2 + ".\n\n");

				//#region CheckChoice

				do {
					do {
						Write ("Jesce? (y/N) = ");
						readStr = Console.ReadLine();
					} while (readStr.Length > 1 || readStr.Length < 1);
					yesNOtext = char.Parse(readStr);
					WriteLine(yesNOtext);
				} while (yesNOtext != 'y' && yesNOtext != 'N');
				
				//#regionend

			} while (yesNOtext == 'y');

			Console.Clear ();
		}


		//#region PrivateFunctions

		static double Delta(double a, double b, double c)
		{
			return ((Math.Pow(b,2)) - 4 * a * c);
		}

		static double ReadDouble()
		{
			return double.Parse (Console.ReadLine ());
		}

		static void Delay(int _msec)
		{
			System.Threading.Thread.Sleep(_msec);
		}

		static void WriteLine(string str){
			Console.WriteLine (str);
		}

		static void WriteLine(char chr){
			Console.WriteLine (chr);
		}

		static void Write(string str){
			Console.Write (str);
		}
		
		//#regionend
	}
}